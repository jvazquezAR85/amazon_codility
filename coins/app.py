'''
Created on Mar 4, 2017

@author: jvazquez
'''


def solution(A):
    """Calculate adjacency

        params:
            A list
        return
            int

    """
    list_size = len(A)
    result = 0
    # compara para arriba
    for i in xrange(list_size - 1):
        if (A[i] == A[i + 1]):
            result = result + 1
    print "Result is {}".format(result)
    r = 0

    for index in xrange(list_size):
        count = 0
        # No usa el primer elemento
        if (index > 0):
            if (A[index - 1] != A[index]):
                count = count - 1
            else:
                count = count + 1
        # No usa el ultimo elemento
        if (index < list_size - 1):
            if (A[index + 1] != A[index]):
                count = count + 1
            else:
                count = count - 1
        r = max(r, count)
    return result + r

# Inicial
monedas = [1, 1, 0, 1, 0, 0]
# monedas = [1, 1, 0, 1, 0, 0, 0, 0, 0, 0]
# monedas = [1, 1, 1, 1, 1, 1, 1, 1]
adjacency = solution(monedas)
print "Adjacency is {}".format(adjacency)
