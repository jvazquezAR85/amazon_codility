'''
Created on Mar 4, 2017

@author: jvazquez
'''


def eq_index(collection):
    """Calculate the equilibrium index of an array

        params:
            collection list (:list: List of integers)
        return
            int

    """
    elements = len(collection)
    elements % 2
    print("{}".format(elements / 2))
    return None


if __name__ == "__main__":
    test_array = [-7, 1, 5, 2, -4, 3, 0]
    eq_number = eq_index(test_array)
    print("{} is the eq number".format(eq_number))
